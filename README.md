Motivation
==========
The motivation came from https://news.ycombinator.com/item?id=16371629 

What I have done so far
=======================
I've made a first attempt at downloading the site by wget and putting it on gitlab. 


Next steps: 
We might want to update this site with any updates upstream. 
An easy way to do it is to use wget. 
Assuming you cloned the repo at ~/src/magpie (among other assumptions), run the following: 

```
cd ~/src/magpie/;wget --recursive --no-parent http://www.amagpiesnest.com/;git add -A;git commit -m "update website from upstream";git push -u origin --all;
```

Pull requests are most welcome. 

